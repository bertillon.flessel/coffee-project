package CoffeeMaker;

import Drink.CommandedDrink;
import Drink.DrinkList;
import DrinkMaker.PrimitiveDrinkMaker;
import DrinkMaker.Reporter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PrimitiveDrinkMakerTest {

    private PrimitiveDrinkMaker drinkMaker=null;

    private Reporter reporter = null;

    @BeforeEach
    void init() {
        drinkMaker = new PrimitiveDrinkMaker();
        reporter = Reporter.getReporter();


    }

    @Test
    void receiveCommandeTest(){
        CommandedDrink drinkToTest= new CommandedDrink(DrinkList.Chocolate,1,true,false);
        String drink =  drinkMaker.receiveCommand(drinkToTest);
        assertEquals("H:1:0",drink);
    }


    @Test
    void receiveCommandeWithNoSugarTest(){
        CommandedDrink drinkToTest= new CommandedDrink(DrinkList.Chocolate,0,true,false);
        String drink =  drinkMaker.receiveCommand(drinkToTest);
        assertEquals("H::",drink);
    }


    @Test
    void sendMessageToCostumerTest(){
        String drink =  drinkMaker.sendMessageToCostumer("Gimme More");
        assertEquals("M:Gimme More",drink);
    }

    @Test
    void receiveCommandeButNotEnoughMoneyTest(){
        CommandedDrink drinkToTest= new CommandedDrink(DrinkList.Chocolate,1,false,false);
        String drink =  drinkMaker.receiveCommand(drinkToTest);
        assertEquals("Not Enough Money",drink);
    }

    @Test
    void receiveCommandeForColdDrinkTest(){
        CommandedDrink drinkToTest= new CommandedDrink(DrinkList.OrangeJuice,1,true,true);
        String drink =  drinkMaker.receiveCommand(drinkToTest);
        assertEquals("O::",drink);
    }

    @Test
    void receiveCommandeTestExtraHot(){
        CommandedDrink drinkToTest= new CommandedDrink(DrinkList.Chocolate,1,true,true);
        String drink =  drinkMaker.receiveCommand(drinkToTest);
        assertEquals("Hh:1:0",drink);
    }

    @Test
    void printDailyReport(){
        CommandedDrink drink = new CommandedDrink(DrinkList.OrangeJuice,1,true,false);
        reporter.counting(drink);
        String report = reporter.simpleReporting();
        assertEquals("Number of drinks sold : 1\n" +
                "Total amount of money earned : 0.6\n" +
                "1 of OrangeJuice were sold\n",report);
    }

    @Test
    void printDailyReportForSeverals(){
        List<CommandedDrink> drinks = Arrays.asList(new CommandedDrink(DrinkList.OrangeJuice,1,true,false),
                new CommandedDrink(DrinkList.Chocolate,1,true,true), new CommandedDrink(DrinkList.Coffee,1,true,true),
                new CommandedDrink(DrinkList.Tea,1,true,true));
        drinks.forEach((e) ->{reporter.counting(e);});
        String report = reporter.simpleReporting();
        assertEquals("Number of drinks sold : 4\n" +
                "Total amount of money earned : 2.1\n" +
                "1 of Tea were sold\n" +
                "1 of OrangeJuice were sold\n" +
                "1 of Chocolate were sold\n" +
                "1 of Coffee were sold\n",report);
    }

}
