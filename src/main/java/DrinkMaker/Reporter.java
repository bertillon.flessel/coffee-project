package DrinkMaker;

import Drink.CommandedDrink;

import java.util.HashMap;
import java.util.Map;

public class Reporter {

    private int nbDrinkSold=0;
    private Map<String, Integer> totalDrinks = new HashMap<String, Integer>();
    private double totalMoneyEarned=0;

    private Reporter(){

    }

    private static Reporter REPORTER = new Reporter();

    private int getNbDrinkSold() {
        return nbDrinkSold;
    }

    private void setNbDrinkSold(int nbDrinkSold) {
        this.nbDrinkSold = nbDrinkSold;
    }

    private double getTotalMoneyEarned() {
        return totalMoneyEarned;
    }

    private void setTotalMoneyEarned(double totalMoneyEarned) {
        this.totalMoneyEarned = totalMoneyEarned;
    }
    public void counting(CommandedDrink drink){
        this.totalMoneyEarned+= drink.getType().getCost();
        this.nbDrinkSold++;
        Integer test = this.totalDrinks.get(drink.getType().getDrinkCode());
        Integer nb = new Integer (0);
        nb = (test!=null) ? test : nb;
        this.totalDrinks.put(drink.getType().name(),++nb);
    }
    public String simpleReporting(){
        StringBuilder rapport = new StringBuilder("Number of drinks sold : "+this.nbDrinkSold + "\nTotal amount of money earned : "+this.totalMoneyEarned+ "\n");
        this.totalDrinks.forEach((k, v) -> rapport.append(v).append(" of ").append(k).append(" were sold\n"));
        return rapport.toString();
    }

    public static Reporter getReporter(){
        if (REPORTER == null)
        {   REPORTER = new Reporter();
        }
        return REPORTER;
    }
}