package DrinkMaker;

import Drink.CommandedDrink;

public class PrimitiveDrinkMaker {
    static final String  SEPARATOR =":";
    static final String  NOSUGAR ="::";
    static final String  WITHSUGAR =":0";

    private Reporter reporter = Reporter.getReporter();

    public String receiveCommand(CommandedDrink drink){
        reporter.counting(drink);
        return formatDrinkInstruction(drink);
    }

    public String sendMessageToCostumer (String message){

        return formatInformationInstruction(message);
    }

    private String formatDrinkInstruction(CommandedDrink drink){
        StringBuilder stringBuilder = new StringBuilder();
        if(drink.isEnoughMoney()) {
            stringBuilder.append(drink.getType().getDrinkCode());
            if(drink.getType().isColdDrink()) {
                formatColdDrinks(drink, stringBuilder);
            }else {
                formatHotDrinks(drink, stringBuilder);
            }
       } else {
            stringBuilder.append("Not Enough Money");
        }
        return stringBuilder.toString();
    }

        private void formatColdDrinks(CommandedDrink drink,StringBuilder stringBuilder ){
            stringBuilder.append(NOSUGAR);
        }

        private void formatHotDrinks(CommandedDrink drink,StringBuilder stringBuilder ){
            if(drink.isExtraHot()){
                stringBuilder.append("h");
            }
            if (drink.getSugarAmount() == 0) {
                stringBuilder.append(NOSUGAR);
            } else {
                stringBuilder.append(SEPARATOR).append(drink.getSugarAmount()).append(WITHSUGAR);
            }
        }

    private String formatInformationInstruction(String message){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("M").append(SEPARATOR).append(message);
        return stringBuilder.toString();
    }



}
