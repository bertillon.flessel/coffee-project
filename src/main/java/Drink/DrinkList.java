package Drink;

public enum DrinkList {

    Coffee("C",0.6,false),Tea("T",0.4,false),Chocolate("H",0.5,false),OrangeJuice("O",0.6,true);
    private String drinkCode;
    private Double cost;
    private boolean coldDrink;

    DrinkList(String drinkCode,Double cost, boolean coldDrink) {
        this.drinkCode = drinkCode;
        this.cost = cost;
        this.coldDrink = coldDrink;
    }

    DrinkList() {
    }

    public String getDrinkCode() {
        return drinkCode;
    }

    public void setDrinkCode(String drinkCode) {
        this.drinkCode = drinkCode;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public boolean isColdDrink() {
        return coldDrink;
    }

    public void setColdDrink(boolean coldDrink) {
        this.coldDrink = coldDrink;
    }
}
