package Drink;

public class CommandedDrink {
    private DrinkList type;
    private int sugarAmount;
    private boolean enoughMoney;
    private boolean extraHot;


    public CommandedDrink(DrinkList type, int sugarAmount, boolean enoughMoney, boolean extraHot) {
        this.type = type;
        this.sugarAmount = sugarAmount;
        this.enoughMoney = enoughMoney;
        this.extraHot = extraHot;

    }

    public CommandedDrink() {
    }

    public DrinkList getType() {
        return type;
    }

    public void setType(DrinkList type) {
        this.type = type;
    }

    public int getSugarAmount() {
        return sugarAmount;
    }

    public void setSugarAmount(int sugarAmount) {
        this.sugarAmount = sugarAmount;
    }

    public boolean isEnoughMoney() {
        return enoughMoney;
    }

    public void setEnoughMoney(boolean enoughMoney) {
        this.enoughMoney = enoughMoney;
    }

    public boolean isExtraHot() {
        return extraHot;
    }

    public void setExtraHot(boolean extraHot) {
        this.extraHot = extraHot;
    }
}
